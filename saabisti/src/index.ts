import { Context } from 'aws-lambda';
import {Poppler} from 'node-poppler';

export const handler = async (event: Record<string, any>, context: Context): Promise<string> => {
    console.log(`Event: ${JSON.stringify(event, null, 2)}`);
    console.log(`Context: ${JSON.stringify(context, null, 2)}`);
    new Poppler('/usr/bin');

    return 'hello from lambda handler';
};