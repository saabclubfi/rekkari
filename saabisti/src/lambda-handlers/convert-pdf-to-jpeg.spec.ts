
import handler from './convert-pdf-to-jpeg';

describe('Saabisti Lambda handlers', () => {
    it('returns a hello string', () => {

        expect(handler()).toEqual('Hello from AWS Lambda using Node!')
    })
})