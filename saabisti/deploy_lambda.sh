
env="${1:-}"
version="${2:-}"

if [ -z "${env}" ] || [ -z "${version}" ] || [ $env != "dev" -a $env != "prod" ]; then
    echo "Usage: bash deploy_lambda.sh <dev|prod>"
    exit -1
fi

AWSCLI=aws
AWSCLI_CMD="cloudformation deploy"
AWSCLI_EXTRA_ARGS=""

set -eux

AWSCLI_EXTRA_ARGS=""
if [ $1 == "prod" ]; then
    AWSCLI_EXTRA_ARGS="--profile prodaccess"
fi

export AWS_DEFAULT_REGION=eu-north-1

${AWSCLI} ${AWSCLI_CMD} ${AWSCLI_EXTRA_ARGS} \
    --template-file "template.yml" \
    --capabilities CAPABILITY_IAM \
    --stack-name "Saabisti-$env-Handlers" \
    --parameter-overrides "DeploymentEnvironment=$env" "ContainerVersion=$version"\
    --no-fail-on-empty-changeset
