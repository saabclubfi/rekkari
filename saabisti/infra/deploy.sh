
env="${1:-}"
stack="${2:-}"

AWSCLI=aws
AWSCLI_CMD="cloudformation deploy"
AWSCLI_EXTRA_ARGS=""

if [ -z "$env" ] || [ -z "$stack" ] || [ $env != "dev" -a $env != "prod" ] || [ $stack != "container-repositories" ]
then
    echo "Usage: bash deploy.sh <dev|prod> container-repositories"
    exit -1
fi

if [ $env == "prod" ]
then
    AWSCLI_EXTRA_ARGS="--profile prodaccess"
fi

set -eux

export AWS_DEFAULT_REGION=eu-north-1

deploy_container-repositories () {
    ${AWSCLI} ${AWSCLI_CMD} ${AWSCLI_EXTRA_ARGS} \
        --template-file "container-repositories.yml" \
        --stack-name "Saabisti-$env-ContainerRepositories" \
        --parameter-overrides "DeploymentEnvironment=$env" \
        --no-fail-on-empty-changeset
}

case "$stack" in
"container-repositories")
    deploy_container-repositories
    ;;
esac
