
env="${1:-}"

if [ -z "$env" ] || [ $env != "dev" -a $env != "prod" ]
then
    echo "Usage: bash build.sh <dev|prod>"
    exit -1
fi

if [ $env == "prod" ]
then
    AWSCLI_EXTRA_ARGS="--profile prodaccess"
fi

set -eux

export AWS_DEFAULT_REGION=eu-north-1

IMAGE_NAME="saabclub-saabisti-${env}"
IMAGE_TAG="$(date +%s)"
docker build -t ${IMAGE_NAME}:${IMAGE_TAG} .

aws ecr get-login-password --region eu-north-1 | docker login --username AWS --password-stdin 471482825679.dkr.ecr.eu-north-1.amazonaws.com

docker tag ${IMAGE_NAME}:${IMAGE_TAG} 471482825679.dkr.ecr.eu-north-1.amazonaws.com/${IMAGE_NAME}:${IMAGE_TAG}
docker tag ${IMAGE_NAME}:${IMAGE_TAG} 471482825679.dkr.ecr.eu-north-1.amazonaws.com/${IMAGE_NAME}:latest

# Remove latest tag because we have immutable tags
aws ecr batch-delete-image --repository-name saabclub-saabisti-dev --image-ids imageTag=latest

docker push 471482825679.dkr.ecr.eu-north-1.amazonaws.com/${IMAGE_NAME}:${IMAGE_TAG}
docker push 471482825679.dkr.ecr.eu-north-1.amazonaws.com/${IMAGE_NAME}:latest

