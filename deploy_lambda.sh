#!/bin/bash

set -euo pipefail

if [ -z "${1:-}" ] || [ $1 != "dev" -a $1 != "prod" ]
then
    echo "Usage: bash deploy_lambda.sh <dev|prod>"
    exit -1
fi

set -eux

AWSCLI_EXTRA_ARGS=""
if [ $1 == "prod" ]
then
    AWSCLI_EXTRA_ARGS="--profile prodaccess"
fi

export AWS_DEFAULT_REGION=eu-west-1

# Package SAM template
sam package ${AWSCLI_EXTRA_ARGS} --template-file sam-template.yml --s3-bucket saabclub-rekkari-depl-$1 --output-template-file packaged-$1.yaml

# Deploy packaged SAM template
sam deploy ${AWSCLI_EXTRA_ARGS} \
    --template-file ./packaged-$1.yaml \
    --stack-name "Rekkari-$1-LambdaHandlers" \
    --parameter-overrides "DeploymentEnvironment=$1" \
    --capabilities CAPABILITY_IAM
