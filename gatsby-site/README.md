# Gatsby site for Saabclub Rekkari

## Develop locally

1. Clone the repo

2. Change into the gatsby site directory and install dependencies

```
npm install
```

3. Setup `.env.development` file with the contents

```sh
GATSBY_USER_POOL_ID=XXXXXX
GATSBY_USER_POOL_WEB_CLIENT_ID=XXXXXX
GATSBY_API_BASE_PATH=https://rekkari-api.dev.saabclub.fi/v1
```

4. Start the Gatsby development server:

```
gatsby develop
```
