const gatsbySourceFilesystem = {
  resolve: 'gatsby-source-filesystem',
  options: {
    name: 'images',
    path: `${__dirname}/src/images`,
  },
};

const gatsbyPluginManifest = {
  resolve: 'gatsby-plugin-manifest',
  options: {
    name: 'gatsby-starter-default',
    short_name: 'starter',
    start_url: '/',
    background_color: '#0070ba',
    theme_color: '#0070ba',
    display: 'minimal-ui',
    icon: 'src/images/saabclub-hockeystick-icon.png', // This path is relative to the root of the site.
  },
};

module.exports = {
  siteMetadata: {
    title: 'Rekkari',
    description: 'SaabClub membership services',
    author: '@gatsbyjs',
  },
  plugins: [
    gatsbySourceFilesystem,
    gatsbyPluginManifest,
    'gatsby-plugin-sharp',
    'gatsby-plugin-sass',
    'gatsby-plugin-image',
  ],
};
