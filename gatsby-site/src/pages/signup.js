import { Auth } from 'aws-amplify';
import { Link } from 'gatsby';
import React, { useState, useEffect } from 'react';
import { useErrorStateWithToaster } from '../hooks/useErrorStateWithToaster';
import { useValidator } from '../hooks/useValidator';
import { isLoggedIn } from '../utils/auth';

import Layout from '../components/Layout';
import FullBackGround from '../components/BackgroundImage';

import Error from '../components/Error';
import errorTranslator from '../utils/errorTranslator';
import {
  validatePassword,
  validateEmail,
  validateCode,
} from '../utils/validators';

import * as styles from './signup.module.scss';
import { useEnterKeyListener } from '../hooks/useEnterKeyListener';
import { toast } from 'react-hot-toast';
import { useRedirect } from '../hooks/useRedirect';

const LOCALSTORAGE_KEY_SIGNUPSTATE = 'signupState';
const STAGE_CONFIRM = 'confirm';
const STAGE_SIGNUP = 'signup';

const SignUp = () => {
  const [email, setEmail] = useState('');
  const [stage, setStage] = useState(STAGE_SIGNUP);

  useEffect(() => {
    try {
      const { stage: storedStage, email: storedEmail } = JSON.parse(
        localStorage.getItem(LOCALSTORAGE_KEY_SIGNUPSTATE),
      );
      setStage(storedStage);
      setEmail(storedEmail);
    } catch {}
  }, []);

  return (
    <FullBackGround>
      <Layout>
        <main className={styles.container}>
          {isLoggedIn() ? (
            <p>Jos haluat tehdä uuden tunnuksen, kirjaudu ensin ulos.</p>
          ) : (
            <>
              {stage === STAGE_SIGNUP && (
                <SignUpComponent
                  email={email}
                  setEmail={setEmail}
                  setStage={setStage}
                />
              )}
              {stage === STAGE_CONFIRM && (
                <ConfirmComponent
                  email={email}
                  setEmail={setEmail}
                  setStage={setStage}
                />
              )}
            </>
          )}
        </main>
      </Layout>
    </FullBackGround>
  );
};

const SignUpComponent = ({ email, setEmail, setStage }) => {
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [error, setError] = useErrorStateWithToaster('');
  const [formInProgress, setFormInProgress] = useState(false);

  const [passwordInvalid, passwordHelpMessage] = useValidator(
    validatePassword,
    password,
  );
  const [emailInvalid, emailHelpMessage] = useValidator(validateEmail, email);

  const [password2Invalid, passwordNotSameMessage] = useValidator(
    (p1, p2) => {
      const isInvalid = p1.lenght === 0 || p1 !== p2;
      const message =
        isInvalid && p2.length > 0 ? 'Salasana ei ole sama kuin ylempänä.' : '';
      return [isInvalid, message];
    },
    password,
    password2,
  );

  const anyFieldInvalid = emailInvalid || passwordInvalid || password2Invalid;
  const anyFieldEmpty = !email || !password || !password2;

  useEffect(() => {
    setError('');
    setFormInProgress(false);
  }, [email, setError]);

  const handleEmailUpdate = (e) => {
    setEmail(e.target.value.toLowerCase());
  };

  const signUpCognitoUser = async () => {
    setFormInProgress(true);
    try {
      await Auth.signUp({ username: email, password, attributes: { email } });
      setError('');
      setStage(STAGE_CONFIRM);
      localStorage.setItem(
        LOCALSTORAGE_KEY_SIGNUPSTATE,
        JSON.stringify({ stage: STAGE_CONFIRM, email }),
      );
    } catch (cognitoError) {
      setError(errorTranslator.tCognitoError(cognitoError));
      console.error('error signing up...', cognitoError);
    }
  };

  useEnterKeyListener(
    [!anyFieldEmpty, !anyFieldInvalid, !formInProgress],
    signUpCognitoUser,
  );

  return (
    <>
      <div>
        <p>
          Aloita syöttämällä toimiva sähköpostiosoite ja itse keksimäsi
          salasana. Seuraavassa vaiheessa saat sähköpostiin varmistuskoodin.
        </p>
        <p>
          Salasanassa on oltava vähintään kymmenen merkkiä. Sen pitää sisältää
          isoja ja pieniä kirjaimia sekä vähintään yhden numeron.
        </p>
        <p className={styles.passwdHelp}>
          Onko sinulla jo tunnus?&nbsp;
          <Link to="/login">Kirjaudu sisään.</Link>
        </p>
        {error && <Error Virhe={error} />}
      </div>
      <div className={styles.formContainer}>
        <label className={styles.label} htmlFor="email">
          Sähköpostiosoite
        </label>
        <input
          type="search"
          onChange={handleEmailUpdate}
          placeholder="tunnus@joku.osoite"
          name="email"
          value={email}
          className={`${styles.input} ${
            emailInvalid ? styles.invalidInput : styles.validInput
          }`}
        />
        <p className={styles.passwdHelp}>{emailHelpMessage}</p>
        <label htmlFor="password" className={styles.label}>
          Salasana
        </label>
        <input
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Vähintään 10 merkkiä."
          name="password"
          value={password}
          type="password"
          className={`${styles.input} ${
            passwordInvalid ? styles.invalidInput : styles.validInput
          }`}
        />
        <p className={styles.passwdHelp}>{passwordHelpMessage}</p>
        <label className={styles.label} htmlFor="password2">
          Salasana uudestaan
        </label>
        <input
          onChange={(e) => setPassword2(e.target.value)}
          name="password2"
          value={password2}
          type="password"
          className={`${styles.input} ${
            password2Invalid ? styles.invalidInput : styles.validInput
          }`}
        />
        <p className={styles.passwdHelp}>{passwordNotSameMessage}</p>
        <button
          className={styles.button}
          onClick={signUpCognitoUser}
          disabled={anyFieldEmpty || anyFieldInvalid || formInProgress}
        >
          <span className={styles.buttonText}>Tee tunnus</span>
        </button>
      </div>
    </>
  );
};

const ConfirmComponent = ({ email, setStage }) => {
  const [authCode, setAuthCode] = useState('');
  const [error, setError] = useErrorStateWithToaster('');
  const [formInProgress, setFormInProgress] = useState(false);

  const { redirect: toLoginAfter } = useRedirect('/login');

  const [confirmationCodeInvalid, confirmationCodeHelpMessage] = useValidator(
    validateCode,
    authCode,
  );
  const confCodeValidityClass = confirmationCodeInvalid
    ? styles.invalidInput
    : styles.validInput;

  useEffect(() => {
    setError('');
    setFormInProgress(false);
  }, [authCode, setError]);

  const confirmCognitoSignUp = async () => {
    setFormInProgress(true);
    console.log(`Confirming with email ${email} and authcode ${authCode}`);
    try {
      await Auth.confirmSignUp(email, authCode);
      toast.success(
        'Tunnuksen luonti onnistui. Siirryt hetken kuluttua kirjautumiseen.',
      );
      setError('');
      toLoginAfter(3000);
      localStorage.removeItem(LOCALSTORAGE_KEY_SIGNUPSTATE);
    } catch (cognitoError) {
      setError(errorTranslator.tCognitoError(cognitoError));
      console.error('error confirming signing up...', cognitoError);
    }
  };

  useEnterKeyListener(
    [!confirmationCodeInvalid, email, !formInProgress],
    confirmCognitoSignUp,
  );

  const requestNewCode = async () => {
    setFormInProgress(true);
    try {
      await Auth.resendSignUp(email);
      setFormInProgress(false);
      toast.success(`Tilasit sähköpostiin ${email} uuden varmistuskoodin.`);
    } catch (cognitoError) {
      setError(errorTranslator.tCognitoError(cognitoError));
      console.error('error requesting new code...', cognitoError);
    }
  };

  const resetStage = () => {
    localStorage.removeItem(LOCALSTORAGE_KEY_SIGNUPSTATE);
    setStage(STAGE_SIGNUP);
  };

  return (
    <>
      <div>
        <p>
          Odota, että saat sähköpostin osoitteesta tunnus@saabclub.fi. Syötä sen
          jälkeen sähköpostista löytyvä varmistuskoodi. Koodissa on pelkkiä
          numeroita.
        </p>
        <p>
          Jos koodia ei ala kuulua, tarkasta roskapostikansio tai pyydä uusi
          koodi.
        </p>
        <p>
          Jos sattui käymään niin, että annoit edellisessä kohdassa kuitenkin
          väärän sähköpostiosoitteeseen, ei hätää. Palaa alkuun ja yritä
          uudestaan.
        </p>
        {error && <Error Virhe={error} />}
      </div>
      <div className={styles.formContainer}>
        <label htmlFor="email">Sähköpostiosoite</label>
        <input
          readOnly
          placeholder="tunnus@joku.osoite"
          name="email"
          value={email}
          className={styles.input}
        />
        <label htmlFor="authCode">Varmistuskoodi</label>
        <input
          onChange={(e) => setAuthCode(e.target.value)}
          placeholder="6-numeroinen koodi"
          name="authCode"
          value={authCode}
          className={`${styles.input} ${confCodeValidityClass}`}
        />
        <p className={styles.passwdHelp}>{confirmationCodeHelpMessage}</p>
        <button
          className={styles.button}
          onClick={confirmCognitoSignUp}
          disabled={confirmationCodeInvalid || !email || formInProgress}
        >
          <span className={styles.buttonText}>Varmista</span>
        </button>
        <button
          className={`${styles.button} ${styles.buttonMinor}`}
          onClick={requestNewCode}
          disabled={!email || formInProgress}
        >
          <span className={styles.buttonText}>Uusi koodi</span>
        </button>
        <button
          className={`${styles.button} ${styles.buttonMinor}`}
          onClick={() => resetStage()}
          disabled={formInProgress}
        >
          <span className={styles.buttonText}>Alkuun</span>
        </button>
      </div>
    </>
  );
};

export default SignUp;
