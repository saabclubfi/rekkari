import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/Layout';

import FullBackGround from '../components/BackgroundImage';

import * as styles from './index.module.scss';

const IndexPage = () => {
  return (
    <FullBackGround>
      <Layout>
        <main className={styles.container}>
          <p>
            Tällä sivustolla voit tehdä itsellesi Saabclub-tunnuksen ja
            aktivoida jäsenpalvelut verkossa.
          </p>
          <p>
            Tarvitset tunnuksen tekemiseen toimivan sähköpostiosoitteen. Kun
            olet ensin tehnyt tunnuksen, voit kirjautua sivustolle ja aktivoida
            palvelut käyttämällä jäsenmaksukirjeen mukana saamaasi avainkoodia.
          </p>
          <p>
            Tulevaisuudessa pystyt maksamaan täällä jäsenmaksusi ja muuttamaan
            osoite- tai kalustotietojasi.
          </p>
          <button className={styles.linkButton}>
            <Link to="/signup">Aloita</Link>
          </button>
        </main>
      </Layout>
    </FullBackGround>
  );
};

export default IndexPage;

export { Head } from '../components/Head';
