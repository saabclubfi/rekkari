import React from 'react';
import Layout from '../components/Layout';

const NotFoundPage = () => (
  <Layout>
    <h1>Sivua ei löydy</h1>
  </Layout>
);

export default NotFoundPage;
