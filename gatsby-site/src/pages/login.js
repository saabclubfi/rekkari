import React, { useState, useEffect } from 'react';
import { Link } from 'gatsby';
import { navigate } from 'gatsby';
import { setUser, isLoggedIn } from '../utils/auth';
import Error from '../components/Error';
import { Auth } from 'aws-amplify';
import { useErrorStateWithToaster } from '../hooks/useErrorStateWithToaster';
import { useEnterKeyListener } from '../hooks/useEnterKeyListener';

import Layout from '../components/Layout';
import FullBackGround from '../components/BackgroundImage';

import errorTranslator from '../utils/errorTranslator';

import * as styles from './login.module.scss';
import { toast } from 'react-hot-toast';
import { useRedirect } from '../hooks/useRedirect';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useErrorStateWithToaster('');
  const [formInProgress, setFormInProgress] = useState(false);

  const { redirect: goHomeAfter } = useRedirect('/home');

  useEffect(() => {
    setFormInProgress(false);
    setError('');
  }, [username, password, setError]);

  useEffect(() => {
    if (isLoggedIn()) {
      navigate('/home');
    }
  }, []);

  const cognitoLogin = async () => {
    try {
      setFormInProgress(true);
      await Auth.signIn(username, password);
      const user = await Auth.currentAuthenticatedUser();
      const userInfo = {
        ...user.attributes,
        username: user.username,
      };
      setUser(userInfo);
      toast.success(
        'Kirjautuminen onnistui. Siirryt hetken kuluttua eteenpäin.',
      );
      goHomeAfter(3000);
    } catch (cognitoError) {
      setError(errorTranslator.tCognitoError(cognitoError));
    }
  };

  useEnterKeyListener([username, password, !formInProgress], cognitoLogin);

  return (
    <FullBackGround>
      <Layout>
        <main className={styles.container}>
          {error && <Error Virhe={error} />}
          <div className={styles.formContainer}>
            <label htmlFor="username">Sähköpostiosoite</label>
            <input
              onChange={(e) => setUsername(e.target.value)}
              placeholder="tunnus@osoite.com"
              name="username"
              value={username}
              className={styles.input}
            />
            <label htmlFor="password">Salasana</label>
            <input
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Salasanasi"
              name="password"
              value={password}
              type="password"
              className={styles.input}
            />
            <button
              className={styles.button}
              disabled={!username || !password || formInProgress}
              onClick={cognitoLogin}
            >
              <span className={styles.buttonText}>Kirjaudu</span>
            </button>
          </div>
          <p className={styles.passwdHelp}>
            Eikö sinulla ole tunnusta?&nbsp;
            <Link to="/signup">Aloita tekemällä uusi.</Link>
          </p>
        </main>
      </Layout>
    </FullBackGround>
  );
};

export default Login;
