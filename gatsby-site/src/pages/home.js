import React, { useState, useEffect } from 'react';
import { Router, Link } from '@reach/router';
import { Auth } from 'aws-amplify';
import { navigate } from 'gatsby';

import Layout from '../components/Layout';
import Activate from '../components/Activate';
import PrivateRoute from '../components/PrivateRoute';
import { isLoggedIn } from '../utils/auth';
import FullBackGround from '../components/BackgroundImage';

import * as styles from './home.module.scss';

const Home = () => (
  <FullBackGround>
    <Layout>
      <Router basepath="/home">
        <HomeComponent path="/" />
        <PrivateRoute path="/activate" component={Activate} />
      </Router>
    </Layout>
  </FullBackGround>
);

const HomeComponent = () => {
  const [email, setEmail] = useState('');
  const [memberId, setMemberId] = useState('');

  useEffect(() => {
    async function fetchUser() {
      try {
        const user = await Auth.currentUserInfo();
        const userInfo = {
          ...user.attributes,
        };
        const { email, 'custom:memberId': memberIdFromCognito } = userInfo;
        email && setEmail(email);
        memberIdFromCognito && setMemberId(memberIdFromCognito);
      } catch (cognitoError) {
        console.log('Failed: ', cognitoError);
      }
    }
    if (!isLoggedIn()) {
      navigate('/login');
      return undefined;
    }
    fetchUser();
  }, []);

  return (
    <main className={styles.container}>
      <div>
        <p>
          Olet kirjautunut käyttäjänä:
          <br />
          {email}
        </p>
        {memberId ? (
          <>
            <p>
              Jäsennumero: {memberId}
              <br />
              Olet aktivoinut jäsenpalvelut.
            </p>
            <p>
              Tulevaisuudessa voit ylläpitää jäsenrekisterin tietoja ja maksaa
              jäsenmaksusi tällä sivustolla. Tällä kertaa kaikki on valmista ja
              voit kirjautua ulos.
            </p>
          </>
        ) : (
          <button className={styles.linkButton}>
            <Link to="/home/activate">Aktivoi jäsenpalvelut</Link>
          </button>
        )}
      </div>
    </main>
  );
};

export default Home;
