const tCognitoError = {
  tCognitoError(errorMessage) {
    const { code = '' } = errorMessage;
    switch (code) {
      case 'UsernameExistsException':
        return 'Käyttämälläsi sähköpostiosoitteella on jo olemassa tunnus.';

      case 'CodeMismatchException':
        return 'Koodi ei ole oikein. Katso sähköpostista lähettäjän tunnus@saabclub.fi lähettämä koodi ja liitä se tähän.';

      case 'NotAuthorizedException':
        return 'Väärä käyttäjätunnus tai salasana.';

      default:
        return 'Bensapumpussa on roskaa. Ota yhteys ylläpitoon.';
    }
  },
};

export default tCognitoError;
