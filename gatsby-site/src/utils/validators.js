export const validatePassword = password => {
  if (password.length === 0) {
    return [true, ''];
  }
  if (password.length < 10) {
    return [true, 'Lisää vielä pituutta.'];
  }
  const capitalRegex = /[A-Z]/;
  const numberRegex = /[0-9]/;
  if (!capitalRegex.test(password) && !numberRegex.test(password)) {
    return [true, 'Lisää vielä yksi iso kirjain ja numero.'];
  }
  if (!capitalRegex.test(password)) {
    return [true, 'Lisää vielä ainakin yksi iso kirjain.'];
  }
  if (!numberRegex.test(password)) {
    return [true, 'Lisää vielä ainakin yksi numero.'];
  }
  return [false, ''];
};

export const validateEmail = email => {
  const emailRegex = /^.+@.+\..+/;
  if (email.length === 0) {
    return [true, ''];
  }
  if (!emailRegex.test(email)) {
    return [true, 'Sähköpostiosoite ei näytä oikealta.'];
  }
  return [false, ''];
};

export const validateCode = authCode => {
  const memberCodeRegex = /.{3,7}-/;
  const confCodeRegex = /^\d{6}$/;
  if (authCode.length === 0) {
    return [true, ''];
  }
  if (memberCodeRegex.test(authCode)) {
    return [true, 'Älä anna tähän jäsenkirjeen koodia.'];
  }
  if (confCodeRegex.test(authCode)) {
    return [false, ''];
  }
  return [true, 'Anna kuusi numeroa.'];
};
