import { navigate } from 'gatsby';
import { useState } from 'react';
import { useTimeout } from 'usehooks-ts';

export const useRedirect = (path) => {
  const [redirectTimeout, setRedirectTimeout] = useState(null);
  useTimeout(() => navigate(path), redirectTimeout);
  const redirect = (timeout = 0) => setRedirectTimeout(timeout);
  return { redirect };
};
