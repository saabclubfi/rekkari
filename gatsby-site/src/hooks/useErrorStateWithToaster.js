import { useState, useEffect } from 'react';
import { toast } from 'react-hot-toast';

export const useErrorStateWithToaster = () => {
  const [error, setError] = useState('');

  useEffect(() => {
    if (error) {
      toast.error(error);
    }
  }, [error]);

  return [error, setError];
};
