import { useEventListener } from './useEventListener';

const eventType = 'keydown';
const keyCodes = ['Enter', 'NumpadEnter'];

export const useEnterKeyListener = (conditions, action) =>
  useEventListener(
    eventType,
    event => keyCodes.some(code => code === event.code),
    conditions,
    action,
  );
