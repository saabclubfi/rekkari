import { useStaticQuery, graphql } from 'gatsby';

export const useSiteTitle = () => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return data.site.siteMetadata.title;
};
