import { useEffect } from 'react';

export const useEventListener = (eventType, filterFn, conditions, action) => {
  useEffect(() => {
    const isFalsy = (condition) => condition == false;
    const listener = (event) =>
      filterFn(event) && !conditions.some(isFalsy) && action();

    document.addEventListener(eventType, listener);
    return () => document.removeEventListener(eventType, listener);
  }, [eventType, filterFn, conditions, action]);
};
