import React from 'react';
import PropTypes from 'prop-types';

import Header from './Header';
import './layout.css';
import './BackgroundImage';
import { useSiteTitle } from '../hooks/useTitle';
import { Toaster } from 'react-hot-toast';

const Layout = ({ children }) => {
  const siteTitle = useSiteTitle();

  return (
    <>
      <Header siteTitle={siteTitle} />
      <Toaster />
      <div
        style={{
          margin: '0 auto',
          maxWidth: 720,
          padding: '0px 1.0875rem 1.45rem',
          paddingTop: 0,
        }}
      >
        {children}
      </div>
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
