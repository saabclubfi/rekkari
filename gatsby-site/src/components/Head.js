import React from 'react';
import { useSiteTitle } from '../hooks/useTitle';

export const Head = () => {
  const siteTitle = useSiteTitle();

  return (
    <>
      <title>{siteTitle}</title>
      <meta name="description" content="Sample" />
      <meta name="keywords" content="sample, something" />
    </>
  );
};
