import { Auth } from 'aws-amplify';
import { Link } from 'gatsby';
import React from 'react';
import { navigate } from 'gatsby';

import { logout, isLoggedIn } from '../utils/auth';

const Header = ({ siteTitle }) => (
  <div
    style={{
      marginBottom: '1.45rem',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 720,
        paddingTop: '5rem',
        textAlign: 'center',
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link to="/" style={styles.headerTitle}>
          {siteTitle}
        </Link>
      </h1>
      {isLoggedIn() && (
        <div
          onClick={() =>
            Auth.signOut()
              .then(logout(() => navigate('/login')))
              .catch((error) => console.log(error))
          }
          style={styles.link}
          role="button"
          tabIndex={0}
          onKeyDown={() => {}}
        >
          Kirjaudu ulos
        </div>
      )}
    </div>
  </div>
);

const styles = {
  headerTitle: {
    color: 'hsl(207, 9%, 79%)',
    textDecoration: 'none',
  },
  link: {
    cursor: 'pointer',
    color: 'hsl(207, 9%, 79%)',
    textDecoration: 'underline',
  },
};

export default Header;
