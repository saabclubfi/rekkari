import React from 'react';

import { StaticImage } from 'gatsby-plugin-image';

const FullBackground = ({ children }) => {
  return (
    <div style={{ display: 'grid' }}>
      <StaticImage
        style={{
          gridArea: '1/1',
          minHeight: '100vh',
        }}
        src="../images/lahti-xtreme-2015.jpg"
        id="fullscreenbg"
        placeholder="blurred"
        alt=""
        role="img"
        formats={['auto', 'webp', 'avif']}
      />
      <div
        style={{
          gridArea: '1/1',
          position: 'relative',
        }}
      >
        {children}
      </div>
    </div>
  );
};

export { FullBackground as default };
