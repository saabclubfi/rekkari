import { Auth } from 'aws-amplify';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Link } from 'gatsby';
import { useErrorStateWithToaster } from '../hooks/useErrorStateWithToaster';
import { useEnterKeyListener } from '../hooks/useEnterKeyListener';

import Error from './Error';

import * as styles from './activate.module.scss';
import { toast } from 'react-hot-toast';
import { useRedirect } from '../hooks/useRedirect';

const accountApiEndpoint = process.env.GATSBY_API_BASE_PATH;

const Activate = () => {
  const [memberId, setMemberId] = useState('');
  const [idOk, setIdOk] = useState(false);
  const [code, setCode] = useState('');
  const [codeOk, setCodeOk] = useState(false);
  const [error, setError] = useErrorStateWithToaster('');
  const [formInProgress, setFormInProgress] = useState(false);

  const { redirect: goHomeAfter } = useRedirect('/home');

  useEffect(() => {
    setIdOk(/^\d{1,5}$/.test(memberId));
    setCodeOk(/[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}/.test(code));
  }, [memberId, code]);

  const handleCodeUpdate = (e) => {
    setError('');
    setCode(e.target.value.toUpperCase());
  };

  const handleIdUpdate = (e) => {
    setError('');
    setMemberId(e.target.value);
  };

  const activateMember = async () => {
    try {
      setFormInProgress(true);
      const session = await Auth.currentSession();
      const response = await axios.post(
        `${accountApiEndpoint}/member/${memberId}/activate`,
        {
          secret: code,
        },
        {
          headers: {
            Authorization: `Bearer ${session.getIdToken().getJwtToken()}`,
          },
        },
      );
      if (response.data.status === 'success') {
        setError('');
        toast.success('Palvelujen aktivointi onnistui. Homma kunnossa.');
        goHomeAfter(3000);
      } else {
        setError('Väärä aktivointikoodi.');
        setFormInProgress(false);
      }
    } catch (error) {
      setFormInProgress(false);
      setError('Ongelma aktivoinnissa. Yritä hetken kuluttua uudestaan.');
      console.error('error activating...', error);
    }
  };

  useEnterKeyListener([idOk, codeOk, !formInProgress], activateMember);

  return (
    <main className={styles.container}>
      <div>
        <p>
          Tässä vaiheessa tarvitset jäsenkirjeen mukana saamasi avainkoodin ja
          jäsennumerosi. Numeron löydät joko samaisesta kirjeestä tai
          jäsenkortistasi.
        </p>
        <p>
          Jos sinulta puuttuu joko jäsenumero tai avainkoodi, ota yhteyttä
          jäsenrekisteriin (jasenasiat@saabclub.fi).
        </p>
        <p className={styles.passwdHelp}>
          Oletko jo aktivoinut jäsenpalvelut?&nbsp;
          <Link to="/home">Palaa jäsentietoihin.</Link>
        </p>
        {error && <Error Virhe={error} />}
      </div>
      <div className={styles.formContainer}>
        <label htmlFor="memberId">Jäsennumero</label>
        <input
          onChange={handleIdUpdate}
          placeholder="12345"
          name="memberId"
          value={memberId}
          className={`${styles.input} ${styles.inputJasennumero} ${
            idOk ? styles.validInput : styles.invalidInput
          }`}
        />
        <label htmlFor="code">Avainkoodi jäsenkirjeestä</label>
        <input
          onChange={handleCodeUpdate}
          placeholder="XXXXX-XXXXX-XXXXX-XXXXX"
          name="code"
          value={code}
          type="text"
          className={`${styles.input} ${
            codeOk ? styles.validInput : styles.invalidInput
          }`}
        />
        <button
          disabled={formInProgress || !codeOk || !idOk}
          className={styles.button}
          onClick={activateMember}
        >
          <span className={styles.buttonText}>Aktivoi</span>
        </button>
      </div>
    </main>
  );
};

export { Activate as default };
