import React from 'react';

const Error = props => (
  <div>
    {Object.entries(props).map(([err, val], i) => (
      <p key={i}>
        <strong>{err}: </strong>
        {typeof val === 'string' ? val : JSON.stringify(val, '', ' ')}
      </p>
    ))}
  </div>
);

export default Error;
