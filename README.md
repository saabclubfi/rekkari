# Saabclub Rekkari

Saabclub Rekkari


## Deployment

### Deploying a new environment

1. Create ACM certificates
2. Create SES identity
3. Deploy SNS topics
4. Deploy S3 buckets
5. Deploy CloudFront
6. Deploy Lambda
7. Deploy Cognito
8. Deploy DynamoDB

### Notes
If the API returns 401 while the same token works in API GW Console Authorizer test,
try this: https://stackoverflow.com/a/53841869/9213658
