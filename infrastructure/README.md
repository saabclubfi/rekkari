# Deploying the infrastructure

This is currently somewhat complicated due to crossreferences between resources in different stacks.

The sns topics and deployment buckets must be there first. Then, the lambda stack must be deployed.

1. Have AWS admin credentials set in your shell
2. Deploy the sns topics: `bash deploy.sh <dev|prod> sns`
3. Deploy the buckets: `bash deploy.sh <dev|prod> buckets`
4. Build the lambda: `cd ../lambda; npm run build`
5. Deploy the lambda: `cd ..; bash deploy_lambda.sh <dev|prod>`
6. Deploy the cognito stuff: `bash deploy.sh <dev|prod> cognito`
7. Deploy the DynamoDB tables: `bash deploy.sh <dev|prod> dynamo`

## Production deployment

Have a profile like this in your ~.aws/config:

```
[profile prodaccess]
    role_arn = arn:aws:iam::<prod-account-id>:role/ProductionAccountAccessRole
    ### If using credentials file
    source_profile = default
    ### If using credential environment variables
    # source_profile = Environment ### if
```

Execute scripts as usual.
