
env="${1:-}"
stack="${2:-}"

AWSCLI=aws
AWSCLI_CMD="cloudformation deploy"
AWSCLI_EXTRA_ARGS=""

if [ -z "$env" ] || [ -z "$stack" ] || [ $env != "dev" -a $env != "prod" ] || [ $stack != "buckets" -a $stack != "cloudfront" -a $stack != "cognito" -a $stack != "dynamo" -a $stack != "sns" ]
then
    echo "Usage: bash deploy.sh <dev|prod> <buckets|cloudfront|cognito|dynamo|sns>"
    exit -1
fi

if [ $env == "prod" ]
then
    AWSCLI_EXTRA_ARGS="--profile prodaccess"
fi

set -eux

# Cognito is not yet available in Stockholm.
# Deploy everything in Ireland to keep things simple.
export AWS_DEFAULT_REGION=eu-west-1

deploy_buckets () {
    ${AWSCLI} ${AWSCLI_CMD} ${AWSCLI_EXTRA_ARGS} \
        --template-file "buckets.yml" \
        --stack-name "Rekkari-$env-Buckets" \
        --parameter-overrides "DeploymentEnvironment=$env" \
        --no-fail-on-empty-changeset
}

deploy_cognito () {
    ${AWSCLI} ${AWSCLI_CMD} ${AWSCLI_EXTRA_ARGS} \
        --template-file "cognito.yml" \
        --stack-name "Rekkari-$env-Cognito" \
        --parameter-overrides "DeploymentEnvironment=$env" \
        --no-fail-on-empty-changeset \
        --capabilities CAPABILITY_IAM
}

deploy_dynamo () {
    ${AWSCLI} ${AWSCLI_CMD} ${AWSCLI_EXTRA_ARGS} \
        --template-file "dynamodb.yml" \
        --stack-name "Rekkari-$env-DynamoDB" \
        --parameter-overrides "DeploymentEnvironment=$env" \
        --no-fail-on-empty-changeset \
        --capabilities CAPABILITY_IAM
}

deploy_cloudfront () {
    ${AWSCLI} ${AWSCLI_CMD} \
        --template-file "cloudfront.yml" \
        --stack-name "Rekkari-$env-CloudFront" \
        --parameter-overrides "DeploymentEnvironment=$env" \
        --no-fail-on-empty-changeset
}

deploy_sns () {
    ${AWSCLI} ${AWSCLI_CMD} ${AWSCLI_EXTRA_ARGS} \
        --template-file "sns.yml" \
        --stack-name "Rekkari-$env-Sns" \
        --parameter-overrides "DeploymentEnvironment=$env" \
        --no-fail-on-empty-changeset
}

case "$stack" in
"buckets")
    deploy_buckets
    ;;
"cloudfront")
    deploy_cloudfront
    ;;
"cognito")
    deploy_cognito
    ;;
"dynamo")
    deploy_dynamo
    ;;
"sns")
    deploy_sns
    ;;
esac
