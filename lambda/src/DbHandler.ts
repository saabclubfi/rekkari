import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import {
  DeleteCommand,
  DynamoDBDocumentClient,
  GetCommand,
  PutCommand,
} from '@aws-sdk/lib-dynamodb';
import {
  AssumeRoleCommand,
  AssumeRoleCommandOutput,
  STSClient,
} from '@aws-sdk/client-sts';

import Hashids from 'hashids';

const hashIdLength = 16;
const stsClient = new STSClient({});
const tableName = process.env.DYNAMODB_TABLE_NAME || 'Rekkari-dev-Data';

export default class DbHandler {
  private hashids: Hashids;

  private dynamoDb: DynamoDBClient;

  constructor() {
    this.hashids = new Hashids(tableName, hashIdLength);
    this.dynamoDb = new DynamoDBClient({});
  }

  async putSecret(member_id: number, secret: string, event: any) {
    let stsResponse: AssumeRoleCommandOutput;
    const roleArn = event.requestContext.authorizer.claims['cognito:roles'];
    try {
      stsResponse = await stsClient.send(
        new AssumeRoleCommand({
          RoleArn: roleArn,
          RoleSessionName: 'UserPoolRole',
        }),
      );
      if (!stsResponse.Credentials) {
        throw new Error('Did not receive STS credentials');
      }
    } catch (ex) {
      console.error(`Fail, cannot assume role: ${ex}`);
      throw new Error(`Could not assume role ${roleArn}.`);
    }

    const {
      AccessKeyId: accessKeyId,
      SecretAccessKey: secretAccessKey,
      SessionToken: sessionToken,
    } = stsResponse.Credentials;
    const dynamoDb = new DynamoDBClient({
      credentials: {
        accessKeyId: accessKeyId!,

        secretAccessKey: secretAccessKey!,
        sessionToken,
      },
    });
    const docClient = DynamoDBDocumentClient.from(dynamoDb);

    try {
      return await docClient.send(
        new PutCommand({
          TableName: tableName,
          Item: {
            member_hash: this.hashids.encode(member_id),
            member_id,
            secret,
          },
        }),
      );
    } catch (ex) {
      console.error(`Fail: ${ex}`);
      throw new Error(`Failed to update member ${member_id}`);
    }
  }

  async getSecret(member_id: number): Promise<string> {
    const docClient = DynamoDBDocumentClient.from(this.dynamoDb);

    const response = await docClient.send(
      new GetCommand({
        TableName: tableName,
        Key: {
          PK: 'secret',
          SK: member_id,
        },
        AttributesToGet: ['secret'],
      }),
    );
    return response.Item?.secret || '';
  }

  async deleteSecret(member_id: number) {
    const docClient = DynamoDBDocumentClient.from(this.dynamoDb);

    await docClient.send(
      new DeleteCommand({
        TableName: tableName,
        Key: {
          PK: 'secret',
          SK: member_id,
        },
      }),
    );
  }
}
