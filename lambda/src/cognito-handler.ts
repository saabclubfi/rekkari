import {
  AdminUpdateUserAttributesCommand,
  CognitoIdentityProviderClient,
  ListUsersCommand,
} from '@aws-sdk/client-cognito-identity-provider';

export default class CognitoHandler {
  private cognito: CognitoIdentityProviderClient;

  constructor() {
    this.cognito = new CognitoIdentityProviderClient({});
  }

  async getAllActiveMembers(): Promise<Array<Record<string, any>>> {
    const cognitoResponse = await this.cognito.send(
      new ListUsersCommand({
        UserPoolId: process.env.USER_POOL_ID || '',
      }),
    );
    const users = cognitoResponse.Users || [];
    return users
      .map((user) => {
        const attributes =
          user.Attributes?.reduce(
            (prev, curr) => ({
              ...prev,
              ...(curr.Name ? { [curr.Name]: curr.Value } : {}),
            }),
            {} as Record<string, any>,
          ) ?? {};
        return {
          email: attributes.email,
          created: user.UserCreateDate,
          memberId: attributes['custom:memberId'],
        };
      })
      .filter((user) => user.memberId);
  }

  activateMember(user: string, memberId: number) {
    return this.cognito.send(
      new AdminUpdateUserAttributesCommand({
        UserAttributes: [
          {
            Name: 'custom:memberId',
            Value: `${memberId}`,
          },
        ],
        UserPoolId: process.env.USER_POOL_ID,
        Username: user,
      }),
    );
  }
}
