import { GetObjectCommandOutput } from '@aws-sdk/client-s3';
import { Readable } from 'node:stream';
import S3Client from './S3Client';

describe('S3Client', () => {
  it('extracts the body', async () => {
    const readable = Readable.from(Buffer.from('cafe'));
    await S3Client.resolveBody({
      Body: readable,
    } as GetObjectCommandOutput).caseOf({
      Right: (result) => expect(result).toBe('cafe'),
      Left: (error) => expect(error).toBe(null),
    });
  });
});
