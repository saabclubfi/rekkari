import { EitherAsync } from 'purify-ts';
import Client from 'ssh2-sftp-client';

export type ConnectOptions = Pick<
  Client.ConnectOptions,
  'host' | 'port' | 'privateKey' | 'username'
>;

export default class SftpClient {
  client: Client;

  credentials: ConnectOptions;

  constructor(credentials: ConnectOptions) {
    this.client = new Client();
    this.credentials = credentials;
  }

  private async connect() {
    await this.client.connect({ ...this.credentials });
  }

  put({ file, path }: { file: Buffer; path: string }) {
    return EitherAsync(async ({ throwE }) => {
      try {
        await this.connect()
          .then(() => this.client.put(file, path));
      } catch (error) {
        if (error instanceof Error) {
          return throwE(error.message);
        }

        return throwE('SftpClient.put: unknown error');
      } finally {
        await this.client.end();
      }
    });
  }

  list(path: string) {
    return EitherAsync(async ({ throwE }) => {
      try {
        return await this.connect().then(() => this.client.list(path));
      } catch (error) {
        if (error instanceof Error) {
          return throwE(error.message);
        }

        return throwE('SftpClient.list: unknown error');
      }
    });
  }
}
