/* eslint-disable */

import {
  APIGatewayProxyEvent,
  S3Event,
  S3EventRecord,
  SNSEvent,
  SNSEventRecord,
} from 'aws-lambda';
import DbHandler from './DbHandler';
import CognitoHandler from './cognito-handler';
import SsmClient, { RekkariParameter } from './SsmClient';

import {
  handleAdminPostMemberSecret,
  handlePostMemberActivate,
  handleProcessMembershipRegisterXmlDump,
} from './lambdaHandler';
import { PutCommandOutput } from '@aws-sdk/lib-dynamodb';
import { AdminUpdateUserAttributesCommandOutput } from '@aws-sdk/client-cognito-identity-provider';
import { EitherAsync, Right } from 'purify-ts';
import { Parameter } from '@aws-sdk/client-ssm';

import * as parseJson from './util/parseJson';
import * as MemberParser from './MemberParser';

import S3Client from './S3Client';
import SftpClient from './SftpClient';
import { GetObjectCommandOutput } from '@aws-sdk/client-s3';

let mockDbHandler: Partial<DbHandler>;
let mockCognitoHandler: Partial<CognitoHandler>;
let mockSsmClient: Partial<SsmClient>;
let mockS3Client: Partial<S3Client>;
let mockSftpClient: Partial<SftpClient>;

jest.mock('./SsmClient', () => {
  return jest.fn().mockImplementation(() => {
    return mockSsmClient;
  });
});
jest.mock('./DbHandler', () => {
  return jest.fn().mockImplementation(() => {
    return mockDbHandler;
  });
});
jest.mock('./cognito-handler', () => {
  return jest.fn().mockImplementation(() => {
    return mockCognitoHandler;
  });
});
jest.mock('./S3Client', () => {
  return jest.fn().mockImplementation(() => {
    return mockS3Client;
  });
});
jest.mock('./SftpClient', () => {
  return jest.fn().mockImplementation(() => {
    return mockSftpClient;
  });
});

console.log = jest.fn();

describe('LambdaHandler happy flow cases', () => {
  beforeEach(() => {
    mockDbHandler = {
      putSecret: () => Promise.resolve({} as PutCommandOutput),
      getSecret: () => Promise.resolve('secret'),
      deleteSecret: () => Promise.resolve(),
    };

    mockCognitoHandler = {
      activateMember: () =>
        Promise.resolve({} as AdminUpdateUserAttributesCommandOutput),
    };
  });

  it('calls DB handler to persist a secret POSTed by admin', async () => {
    const spy = jest.spyOn(mockDbHandler, 'putSecret');

    await handleAdminPostMemberSecret(testEvent);

    expect(spy).toHaveBeenCalledTimes(1);
    const [memberId, secret, event] = spy.mock.calls[0];
    expect(memberId).toEqual(1);
    expect(secret).toMatch(/\w\w\w\w\w-\w\w\w\w\w-\w\w\w\w\w-\w\w\w\w\w/);
    expect(event).toMatchObject(testEvent);
  });

  it('calls cognitoHandler to activate a member when member POSTs activate', async () => {
    const spy = jest.spyOn(mockCognitoHandler, 'activateMember');
    await handlePostMemberActivate(testEvent);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenLastCalledWith('test-user', 1);
  });

  it('calls DBhandler to delete the secret after activating a member', async () => {
    const spy = jest.spyOn(mockDbHandler, 'deleteSecret');
    await handlePostMemberActivate(testEvent);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenLastCalledWith(1);
  });

  describe('SNS event invokes member reqister XML dump processing', () => {
    const s3EventFixture: S3Event = {
      Records: [
        {
          s3: {
            s3SchemaVersion: 'irrelevant',
            configurationId: 'irrelevant',
            bucket: {
              name: 'test-bucket-name',
              arn: 'irrelevant',
              ownerIdentity: { principalId: 'irrelevant' },
            },
            object: {
              key: 'test-object-key',
              eTag: 'irrelevant',
              sequencer: 'irrelevant',
              size: 0,
            },
          },
        } as S3EventRecord,
      ],
    };

    const snsEventFixture: SNSEvent = {
      Records: [
        {
          Sns: {
            Message: JSON.stringify(s3EventFixture),
          },
        } as SNSEventRecord,
      ],
    };

    const parameterFixtures: Partial<Record<RekkariParameter, string>> = {
      '/Rekkari/UploaderFilePath': 'path-prefix/',
    };

    beforeEach(() => {
      mockSsmClient = {
        getParameter: ({ name }) =>
          EitherAsync<Error, Parameter>(() =>
            Promise.resolve({
              Value: parameterFixtures[name] ?? 'default-parameter',
            }),
          ),
      };
      jest.useFakeTimers().setSystemTime(new Date('2022-12-25'));
    });

    afterEach(() => {
      jest.useRealTimers();
    });

    it('parses the SNS message', async () => {
      const spy = jest.spyOn(parseJson, 'parseJson');

      await handleProcessMembershipRegisterXmlDump(snsEventFixture);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(snsEventFixture.Records[0].Sns.Message);
    });

    it('reads the S3 object specified by the input event', async () => {
      S3Client.prototype.getObject = jest
        .fn()
        .mockReturnValue(
          EitherAsync<Error, string>(() => Promise.resolve('s3-content')),
        );

      await handleProcessMembershipRegisterXmlDump(snsEventFixture);
      expect(S3Client.prototype.getObject).toHaveBeenCalled();
      expect(S3Client.prototype.getObject).toHaveBeenCalledWith({
        bucket: 'test-bucket-name',
        key: 'test-object-key',
      });
    });

    it('uploads the member list using SFTP', async () => {
      mockSftpClient = {
        put: jest.fn(),
      };
      jest
        .spyOn(MemberParser, 'getRegistry')
        .mockReturnValue(
          EitherAsync<string, MemberParser.Registry>(() =>
            Promise.resolve({
              handles: ['handle1', 'handle2'],
              resignedCount: 0,
              memberCount: 2,
            }),
          ),
        );
      mockS3Client = {
        getObject: jest
          .fn()
          .mockImplementation(() =>
            EitherAsync<unknown, GetObjectCommandOutput>(() =>
              Promise.resolve({} as GetObjectCommandOutput),
            ),
          ),
      };

      S3Client.resolveBody = jest
        .fn()
        .mockReturnValue(EitherAsync(async () => 's3-content'));

      await handleProcessMembershipRegisterXmlDump(snsEventFixture);

      expect(mockSftpClient.put).toHaveBeenCalled();
      expect(mockSftpClient.put).toHaveBeenCalledWith({
        file: Buffer.from('handle1' + '\n' + 'handle2'),
        path: 'path-prefix/Nimimerkit20221225.txt',
      });
    });
  });
});

const testEvent = {
  pathParameters: {
    member_id: '1',
  },
  requestContext: {
    authorizer: {
      claims: {
        ['cognito:username']: 'test-user',
      },
    },
  },
  body: JSON.stringify({
    secret: 'secret',
  }),
} as unknown as APIGatewayProxyEvent;
