import { readFile } from 'fs/promises';
import { getRegistry, parse } from './MemberParser';

const memberDumpFile = 'src/sample/members-dump.xml';

describe('MemberParser', () => {
  test('parse()', async () => {
    const value = await readFile(memberDumpFile);
    await parse(value.toString()).caseOf({
      Right: (members) => expect(members.Jasenet.length).toBe(4),
      Left: (error) => expect(error).toBe(null),
    });
  });

  test('getRegistry()', async () => {
    const value = await readFile(memberDumpFile, { encoding: 'utf-8' });
    await getRegistry(value.toString()).caseOf({
      Right: ({ handles, resignedCount, memberCount }) => {
        expect(memberCount).toBe(4);
        expect(resignedCount).toBe(1);
        expect(handles).toStrictEqual([
          'nimimerkki1',
          'nimimerkki2',
          'nimimerkki3',
        ]);
      },
      Left: (error) => expect(error).toBe(null),
    });
  });
});
