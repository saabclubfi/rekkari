import {
  CognitoIdentityProviderClient,
  ListUsersResponse,
} from '@aws-sdk/client-cognito-identity-provider';
import CognitoHandler from './cognito-handler';

let mockCognitoIdp: Partial<CognitoIdentityProviderClient>;

jest.mock('@aws-sdk/client-cognito-identity-provider', () => ({
  CognitoIdentityProviderClient: jest
    .fn()
    .mockImplementation(() => mockCognitoIdp),
  ListUsersCommand: jest.fn().mockImplementation(() => {}),
}));

const cognitoResponseFixture: ListUsersResponse = {
  Users: [
    {
      Username: '8b5b9699-56bf-4d88-a370-162e61f31cdf',
      Attributes: [
        {
          Name: 'sub',
          Value: '8b5b9699-56bf-4d88-a370-162e61f31cdf',
        },
        {
          Name: 'email_verified',
          Value: 'true',
        },
        {
          Name: 'email',
          Value: 'verified@saabclub.fi',
        },
      ],
      UserCreateDate: new Date('2020-05-24T18:13:35.345000+03:00'),
      UserLastModifiedDate: new Date('2020-05-24T18:13:35.345000+03:00'),
      Enabled: true,
      UserStatus: 'FORCE_CHANGE_PASSWORD',
    },
    {
      Username: '9bf7e60e-fe5e-40ff-9991-ff4de0324363',
      Attributes: [
        {
          Name: 'sub',
          Value: '9bf7e60e-fe5e-40ff-9991-ff4de0324363',
        },
        {
          Name: 'email_verified',
          Value: 'false',
        },
        {
          Name: 'email',
          Value: 'unverified@saabclub.fi',
        },
      ],
      UserCreateDate: new Date('2021-02-12T20:01:17.338000+02:00'),
      UserLastModifiedDate: new Date('2021-02-12T20:01:17.338000+02:00'),
      Enabled: true,
      UserStatus: 'UNCONFIRMED',
    },
    {
      Username: 'cfb14c0d-3f6c-4aec-b52b-af959e7128dc',
      Attributes: [
        {
          Name: 'sub',
          Value: 'cfb14c0d-3f6c-4aec-b52b-af959e7128dc',
        },
        {
          Name: 'custom:memberId',
          Value: '1',
        },
        {
          Name: 'email_verified',
          Value: 'true',
        },
        {
          Name: 'email',
          Value: 'member@gmail.com',
        },
      ],
      UserCreateDate: new Date('2020-05-24T18:43:28.580000+03:00'),
      UserLastModifiedDate: new Date('2020-05-24T21:11:49.883000+03:00'),
      Enabled: true,
      UserStatus: 'CONFIRMED',
    },
  ],
};

describe('getAllActiveMembers', () => {
  beforeEach(() => {
    mockCognitoIdp = {
      send: () => Promise.resolve(cognitoResponseFixture),
    };
  });

  it('returns a list of active members', async () => {
    const result = await new CognitoHandler().getAllActiveMembers();

    expect(result.length).toBe(1);
    const { created, email, memberId } = result[0];
    expect(created).toEqual(new Date('2020-05-24T15:43:28.580Z'));
    expect(email).toEqual('member@gmail.com');
    expect(memberId).toEqual('1');
  });

  describe('when user has no Cognito attributes', () => {
    const cognitoResponseWithoutAttributes = {
      Users: [
        {
          Username: '8b5b9699-56bf-4d88-a370-162e61f31cdf',
          UserCreateDate: new Date('2020-05-24T18:13:35.345000+03:00'),
          UserLastModifiedDate: new Date('2020-05-24T18:13:35.345000+03:00'),
          Enabled: true,
          UserStatus: 'FORCE_CHANGE_PASSWORD',
        },
      ],
    };

    beforeEach(() => {
      mockCognitoIdp = {
        send: () => Promise.resolve(cognitoResponseWithoutAttributes),
      };
    });

    it('does not return the user', async () => {
      const result = await new CognitoHandler().getAllActiveMembers();

      expect(result.length).toBe(0);
    });
  });
});
