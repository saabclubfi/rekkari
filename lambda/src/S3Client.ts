import {
  GetObjectCommand,
  GetObjectCommandOutput,
  S3Client as S3,
} from '@aws-sdk/client-s3';
import { EitherAsync } from 'purify-ts';
import type { Stream } from 'node:stream';
import { getContentsWithTempFile } from './temp-file';

export default class S3Client {
  private client: S3;

  constructor() {
    this.client = new S3({});
  }

  getObject({
    bucket,
    key,
  }: {
    bucket: string;
    key: string;
  }): EitherAsync<string, GetObjectCommandOutput> {
    return EitherAsync(async ({ throwE }) => {
      try {
        return await this.client.send(
          new GetObjectCommand({ Bucket: bucket, Key: key }),
        );
      } catch (error) {
        if (error instanceof Error) {
          return throwE(error.message);
        }

        return throwE('S3Client.getObject: unknown error');
      }
    });
  }

  static resolveBody({
    Body: stream,
  }: GetObjectCommandOutput): EitherAsync<string, string> {
    return EitherAsync(async ({ throwE }) => {
      try {
        return await getContentsWithTempFile(stream as Stream);
      } catch (error) {
        if (error instanceof Error) {
          return throwE(error.message);
        }

        return throwE('S3Client.resolveBody: unknown error');
      }
    });
  }
}
