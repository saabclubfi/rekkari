import type { Stream } from 'node:stream';
import { readFile, writeFile, rm } from 'node:fs/promises';
import { join } from 'node:path';
import { tmpdir } from 'node:os';
import { randomInt } from 'node:crypto';

const getFilePath = () => join(tmpdir(), `tmp_${randomInt(5000).toString()}`);

export const getContentsWithTempFile = (
  data: string | Stream,
  resultEncoding: 'utf8' = 'utf8',
) => {
  const path = getFilePath();

  return writeFile(path, data)
    .then(() => readFile(path, { encoding: resultEncoding }))
    .finally(() => rm(path));
};
