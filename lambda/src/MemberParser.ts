import { Left, Right, EitherAsync } from 'purify-ts';
import {
  Codec,
  string,
  array,
  optional,
  oneOf,
  GetType,
} from 'purify-ts/Codec';
import { Parser } from 'xml2js';

const parser = new Parser({ explicitRoot: false, explicitArray: false });

const str2num = Codec.custom<number>({
  decode: (input: any) =>
    Number.isNaN(Number.parseInt(input, 10))
      ? Left(`${input} cannot be parsed into number`)
      : Right(Number.parseInt(input, 10)),
  encode: (input) => String(input),
});

const CarCodec = Codec.interface({
  jasennumero: str2num,
  ID: str2num,
  Malli: optional(string),
  Tarkenne: optional(string),
  Vuosi: optional(str2num),
  Tila: optional(string),
});

const MemberCodec = Codec.interface({
  Jasennumero: str2num,
  Sukunimi: string,
  Etunimi: string,
  Katuosoite: optional(string),
  Postinumero: optional(string),
  Postitoimipaikka: optional(string),
  Matkapuh: optional(string),
  Liittymisaika: optional(string),
  Jasenmaksut: string,
  Syntymaaika: optional(string),
  Salainen: str2num,
  Sposti: optional(string),
  ID: str2num,
  ERO: str2num,
  OsoiteTuntematon: str2num,
  Maksanut: str2num,
  Jäsenluettelojakelu: str2num,
  Nimimerkki: optional(string),
  EiHaluaLehteä: str2num,
  KorttiPVM: optional(string),
  NimimerkkiVäärin: str2num,
  Tilinumerokysytty: str2num,
  KorttiPVM2: optional(string),
  EiHaluaSpostitiedotteita: str2num,
  Eroilmoitus: str2num,
  Autot: optional(oneOf([array(CarCodec), CarCodec])),
});

const MembersCodec = Codec.interface({
  Jasenet: array(MemberCodec),
});

export const parse = (xml: string) =>
  EitherAsync.fromPromise(() =>
    parser.parseStringPromise(xml).then(MembersCodec.decode),
  );

export interface Registry {
  handles: string[];
  resignedCount: number;
  memberCount: number;
}

type Member = GetType<typeof MemberCodec>;
const isResigned = (member: Member) => member.ERO === 1;
const increase = (num: number) => num + 1;

export const getRegistry = (xml: string): EitherAsync<string, Registry> =>
  parse(xml).map((members) =>
    members.Jasenet.reduce(
      (registry, member) => ({
        handles:
          isResigned(member) || member.Nimimerkki === undefined
            ? [...registry.handles]
            : [...registry.handles, member.Nimimerkki],
        resignedCount: isResigned(member)
          ? increase(registry.resignedCount)
          : registry.resignedCount,
        memberCount: increase(registry.memberCount),
      }),
      { handles: [] as string[], resignedCount: 0, memberCount: 0 },
    ),
  );
