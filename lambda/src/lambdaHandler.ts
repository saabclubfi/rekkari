import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  S3Event,
  SNSEvent,
} from 'aws-lambda';

import { EitherAsync } from 'purify-ts';
import { getSecret } from './util/secretGenerator';
import DbHandler from './DbHandler';
import CognitoHandler from './cognito-handler';
import SsmClient from './SsmClient';
import SftpClient from './SftpClient';
import S3Client from './S3Client';
import { parseJson } from './util/parseJson';
import { formatDate } from './util/dates';
import { getRegistry, Registry } from './MemberParser';

const headers: any = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

export const handleAdminGetMembers = async () => {
  const cognitoHandler = new CognitoHandler();
  const members = await cognitoHandler.getAllActiveMembers();

  return {
    statusCode: 200,
    headers,
    body: JSON.stringify({
      members,
    }),
  };
};

export const handleAdminPostMemberSecret = async (
  event: APIGatewayProxyEvent,
) => {
  const member = event.pathParameters?.member_id;
  const secret = getSecret();
  const dbHandler = new DbHandler();

  if (member && secret && dbHandler) {
    try {
      await dbHandler.putSecret(parseInt(member, 10), secret, event);
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          status: 'success',
          message: {
            member,
            secret,
          },
        }),
      };
    } catch (ex) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          status: 'error',
          message: `Request failed: ${ex}`,
        }),
      };
    }
  }

  return {
    statusCode: 403,
    headers,
    body: JSON.stringify({
      status: 'error',
      message: 'Unauthorized',
    }),
  };
};

export const handlePostMemberActivate = async (event: APIGatewayProxyEvent) => {
  console.log('Event: ', JSON.stringify(event));
  const memberId = parseInt(event.pathParameters?.member_id || '0', 10);
  const secretFromRequest = (event.body && JSON.parse(event.body).secret) || '';
  const user = event.requestContext.authorizer?.claims['cognito:username'];
  const dbHandler = new DbHandler();
  const cognitoHandler = new CognitoHandler();

  let response: APIGatewayProxyResult;

  try {
    const dbSecret = await dbHandler.getSecret(memberId);
    console.log(`Got secret: ${dbSecret}`);
    if (
      memberId &&
      secretFromRequest &&
      dbSecret &&
      secretFromRequest === dbSecret
    ) {
      await cognitoHandler.activateMember(user, memberId);
      console.log(
        `Succesfully marked user ${user} as member ${memberId}. Deleting secret.`,
      );
      await dbHandler.deleteSecret(memberId);
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          status: 'success',
          message: 'Activation successful.',
        }),
      };
    }
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        status: 'error',
        message: 'Activation failed.',
      }),
    };
  } catch (ex) {
    console.error(`Request failed: ${ex}`);
    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        status: 'error',
        message: 'Activation failed.',
      }),
    };
    return response;
  }
};

export const createErrorResponse = (message: unknown) => ({
  statusCode: 200,
  headers,
  body: JSON.stringify({
    status: 'error',
    message,
  }),
});

export const createResponse = (statusCode: number = 200) => ({
  statusCode,
});

const join = (separator: string) => (things: string[]) =>
  things.join(separator);
const formatFile = join('\n');
const buildPath = (prefix: string, filename: string) =>
  `${prefix.replace(/\/+$/, '')}/${filename}.txt`;

const fetchSecrets = (client: SsmClient) =>
  EitherAsync(async () => {
    const [
      { Value: privateKey },
      { Value: username },
      { Value: host },
      { Value: pathPrefix },
    ] = await EitherAsync.rights([
      client.getParameter({
        name: '/Rekkari/UploaderPrivateKey',
        decrypt: true, // not secure string yet
      }),
      client.getParameter({
        name: '/Rekkari/UploaderUsername',
      }),
      client.getParameter({
        name: '/Rekkari/UploaderHost',
      }),
      client.getParameter({
        name: '/Rekkari/UploaderFilePath',
      }),
    ]);

    return {
      privateKey,
      username,
      host,
      pathPrefix,
    };
  });

export const handleProcessMembershipRegisterXmlDump = async (
  snsEvent: SNSEvent,
) => {
  const ssmCliet = new SsmClient();
  const { pathPrefix, ...credentials } = (
    await fetchSecrets(ssmCliet)
  ).unsafeCoerce();
  const sftpClient = new SftpClient(credentials);
  const s3Client = new S3Client();

  const parseSNSEvent = ({ Records: [snsEventRecord] }: SNSEvent) =>
    EitherAsync.liftEither(parseJson<S3Event>(snsEventRecord.Sns.Message));

  const getMembershipRegister = ({ Records: [s3EventRecord] }: S3Event) =>
    s3Client
      .getObject({
        bucket: s3EventRecord.s3.bucket.name,
        key: s3EventRecord.s3.object.key,
      })
      .chain(S3Client.resolveBody);

  const storeHandles = ({ handles, resignedCount, memberCount }: Registry) => {
    const file = Buffer.from(formatFile(handles));
    const fileSize = file.byteLength;
    const fileName = `Nimimerkit${formatDate(new Date())}`;
    const handleCount = handles.length;

    return sftpClient
      .put({
        file,
        path: buildPath(pathPrefix as string, fileName),
      })
      .map(() =>
        JSON.stringify({
          name: fileName,
          size: `${fileSize} bytes`,
          members: memberCount,
          resigned: resignedCount,
          handles: handleCount,
          path: pathPrefix,
        }),
      );
  };

  return parseSNSEvent(snsEvent)
    .chain(getMembershipRegister)
    .chain(getRegistry)
    .chain(storeHandles)
    .caseOf({
      Right: console.info,
      Left: console.error,
    });
};
