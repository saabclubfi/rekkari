import { formatDate } from './dates';

describe('formatDate()', () => {
  test('should return properly formatted string', () => {
    const testDate = new Date(2021, 11, 15);
    expect(formatDate(testDate)).toBe('20211215');
  });
});
