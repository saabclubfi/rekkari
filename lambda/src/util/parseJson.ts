import { Either, Left, Right } from 'purify-ts';
import { tryCatch } from './tryCatch';

export const parseJson = <T>(maybeJson: string): Either<string, T> =>
  tryCatch(
    () => Right(JSON.parse(maybeJson) as T),
    (error) => Left(error instanceof Error ? error.message : 'invalid json'),
  );
