import { getSecret } from './secretGenerator';

test('Properly formatted secret is generated', () => {
  const expectedPattern =
    /^[A-NP-Z1-9]{5}-[A-NP-Z1-9]{5}-[A-NP-Z1-9]{5}-[A-NP-Z1-9]{5}$/;
  const generated = getSecret();
  expect(generated.length).toBeGreaterThan(0);
  expect(generated).toMatch(expectedPattern);
  expect(expectedPattern.test(generated)).toBeTruthy();
});

test('Generating 1000 secrets does not return duplicates', () => {
  const secretSet = new Set<string>();
  for (let i = 1000; i--; i) {
    secretSet.add(getSecret());
  }
  expect(secretSet.size).toBe(1000);
});
