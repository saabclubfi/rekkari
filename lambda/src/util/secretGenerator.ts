const SECRET_CHARACTERS = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
const SNIPPETLENGTH = 5;
const SNIPPETS = 4;

export const getSecret = function (): string {
  const secretParts: string[] = [];
  for (let i = SNIPPETS; i--; i) {
    secretParts.push(getRandomSnippet(SNIPPETLENGTH));
  }
  return secretParts.join('-');
};

const getRandomSnippet = function (length: number): string {
  let secret = '';
  for (let i = length; i--; i) {
    secret += getRandomSecretCharacter();
  }
  return secret;
};

const getRandomSecretCharacter = function (): string {
  const rndint = Math.floor(Math.random() * SECRET_CHARACTERS.length);
  return SECRET_CHARACTERS[rndint];
};
