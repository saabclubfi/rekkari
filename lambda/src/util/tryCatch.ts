export const tryCatch = <R, E>(
  operation: () => R,
  handleError: (error: unknown) => E,
) => {
  try {
    return operation();
  } catch (error) {
    return handleError(error);
  }
};
