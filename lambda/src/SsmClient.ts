import { GetParameterCommand, SSMClient as SSM } from '@aws-sdk/client-ssm';
import { EitherAsync } from 'purify-ts';

export type RekkariParameter =
  | '/Rekkari/UploaderPrivateKey'
  | '/Rekkari/UploaderUsername'
  | '/Rekkari/UploaderHost'
  | '/Rekkari/UploaderFilePath';

export default class SsmClient {
  client: SSM;

  constructor() {
    this.client = new SSM({});
  }

  getParameter({
    name,
    decrypt = true,
  }: {
    name: RekkariParameter;
    decrypt?: boolean;
  }) {
    return EitherAsync(async ({ throwE }) => {
      try {
        const { Parameter: parameter } = await this.client.send(
          new GetParameterCommand({ Name: name, WithDecryption: decrypt }),
        );

        if (!parameter) {
          return throwE('SsmClient.getParameter: invalid parameter');
        }

        return parameter;
      } catch (error) {
        if (error instanceof Error) {
          return throwE(error.message);
        }

        return throwE('SsmClient.getParameter: unknown error');
      }
    });
  }
}
