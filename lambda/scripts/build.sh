#!/bin/bash

set -euo pipefail


# Build AWS Lambda Functions source code
node esbuild.js

# Build packages in Linux container
docker run -it --rm -v${PWD}:/repo node:18 bash /repo/scripts/build_packages.sh

