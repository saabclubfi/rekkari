#!/bin/bash

set -euo pipefail

cd /repo
cp package.json package-lock.json dist

cd dist
npm i --omit dev

