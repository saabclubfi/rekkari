require('esbuild')
  .build({
    entryPoints: ['src/lambdaHandler.ts'],
    bundle: true,
    minify: true,
    sourcemap: true,
    platform: 'node',
    target: ['es2020', 'node18'],
    outfile: 'dist/lambdaHandler.js',
    packages: 'external',
  })
  .catch((e) => {
    console.log(e);
    process.exit(1);
  });
