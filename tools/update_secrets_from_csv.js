
const aws = require('../lambda/node_modules/aws-sdk');

const { promisify } = require('util');

const fs = require('fs');
const members = fs.readFileSync('/home/petteri/secrets-prod-1-10000-final.csv', 'UTF-8');

const docClient = new aws.DynamoDB.DocumentClient();
const docClientPut = promisify(docClient.put).bind(docClient);
const table = 'Rekkari-prod-Data';

const putMember = async ([ memberId, secret ]) => {
    console.log(`putMember() called with ${memberId} and ${secret}`);
    try {
        await docClientPut({
            Item: {
                PK: 'secret',
                SK: memberId,
                secret: secret
            },
            TableName: table
        });

    } catch (ex) {
        console.log('error', ex);
    }
}

let memberCount = 0;
for (let member of members.split(/\n/)) {
    console.log(member);
    const [ memberId, secret ] = member.split(',');
    putMember([parseInt(memberId), secret]);
    memberCount++;
}
console.log(`Added ${memberCount} members.`);


