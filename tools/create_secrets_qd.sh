#!/usr/bin/bash

set -eu

if [ -z "${TOKEN:-}" ]
then
    echo "Please pass the ID token as environment variable TOKEN"
    exit -1
fi

if [ -z "${1:-}" ]
then
    echo "Please pass the first and optionally last IDs as parameters"
    echo "Usage: create_secrets_gd.sh <first> [last]"
fi
FIRST_ID=$1
LAST_ID="${2:-$1}"

ENDPOINT="https://ix6968kuue.execute-api.eu-north-1.amazonaws.com/prod"

for ((member_id=FIRST_ID; member_id <=LAST_ID; member_id++))
do
    POST_URI="${ENDPOINT}/admin/member/${member_id}/secret"
    RESP=$(curl -s -X POST -H "Authorization: ${TOKEN}" $POST_URI )
    mem=$(echo $RESP | jq -r .member)
    secret=$(echo $RESP | jq -r .secret)
    echo "${mem},${secret}"
done
