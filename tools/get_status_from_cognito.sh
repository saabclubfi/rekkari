
AWS_DEFAULT_REGION=eu-west-1 aws --profile prodaccess cognito-idp list-users --user-pool-id eu-west-1_vATrqaOwx | \
    jq -r '.Users[].Attributes | from_entries | select(has("custom:memberId")) | [getpath(["custom:memberId"], ["email"])] | join(",")' | \
    sort -n
